" turn syntax highlighting on
set t_Co=256
syntax on
colorscheme wombat256
set relativenumber
set number
"set spell
" --- OmniCppComplete ---
" -- required --
set nocp " non vi compatible mode
filetype plugin on " enable plugins
set omnifunc=syntaxcomplete#Complete
" -- optional --
" auto close options when exiting insert mode
autocmd InsertLeave * if pumvisible() == 0|pclose|endif
set completeopt=menu,menuone
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow
" -- configs --
let OmniCpp_MayCompleteDot = 1 " autocomplete with .
let OmniCpp_MayCompleteArrow = 1 " autocomplete with ->
let OmniCpp_MayCompleteScope = 1 " autocomplete with ::
let OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
let OmniCpp_NamespaceSearch = 2 " search namespaces in this and included files
let OmniCpp_ShowPrototypeInAbbr = 1 " show function prototype (i.e. parameters) in popup window
" -- ctags --
" map <ctrl>+F12 to generate ctags for current folder:
map <C-F11> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR><CR>
noremap <C-K> :CtrlPMixed <CR>
com TT NERDTreeToggle | TlistToggle
map <F8> :TT <CR>
map <F9> :TlistToggle <CR>
" add current directory's generated tags file to available tags
set tags+=./tags
set tags+=~/.myTags/std3.3.tags
set history=700
filetype indent on
set autoread
set wildmenu
set ignorecase
set hlsearch
set incsearch
set smarttab
map <C-j> <C-W>j
map <C-l> <C-W>l
set runtimepath^=~/.vim/bundle/ctrlp.vim
map <F2> :CtrlPtjump <CR>
map <C-E> :vsplit <CR>
map <Right> :tabnext <CR>
map <Left> :tabprevious <CR>
set mouse=a
noremap ß /
noremap <F3> :!astyle -A1 % <CR> <CR>
noremap <C-F3> :!astyle -A2 % <CR> <CR>
set tabstop=2
set shiftwidth=2
" let Tlist_Use_Horiz_Window = 1
" let Tlist_Auto_Open = 1
let Tlist_Use_Split_Window = 1
autocmd BufEnter * silent! lcd %:p:h
noremap <F3> :!pdflatex %<CR><CR>

vmap Y ygv<Esc>
let g:airline_powerline_fonts = 1
nmap     <C-F>f <Plug>CtrlSFPrompt
vmap     <C-F>f <Plug>CtrlSFVwordPath
vmap     <C-F>F <Plug>CtrlSFVwordExec
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath
nnoremap <C-F>o :CtrlSFOpen<CR>
nnoremap <C-F>t :CtrlSFToggle<CR>
inoremap <C-F>t <Esc>:CtrlSFToggle<CR>
" Highlight all instances of word under cursor, when idle.
" Useful when studying strange source code.
" Type z/ to toggle highlighting on/off.
nnoremap z/ :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>
function! AutoHighlightToggle()
  let @/ = ''
  if exists('#auto_highlight')
    au! auto_highlight
    augroup! auto_highlight
    setl updatetime=4000
    return 0
  else
    augroup auto_highlight
      au!
      au CursorHold * let @/ = '\V\<'.escape(expand('<cword>'), '\').'\>'
    augroup end
    setl updatetime=500
    return 1
  endif
endfunction
" call AutoHighlightToggle()

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'vim-airline/vim-airline'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'ddrscott/vim-side-search'
Plugin 'dyng/ctrlsf.vim'
Plugin 'jaxbot/semantic-highlight.vim'



" All of your Plugins must be added before the following line
call vundle#end()            " required
